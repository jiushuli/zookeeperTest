package com.fangsheng.zookeeper.test;

import lombok.SneakyThrows;
import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 * @创建人: 放生
 * @创建时间: 2021/11/3
 * @描述:
 */
public class ZookeeperTest {

    public static void main(String[] args) throws IOException, InterruptedException, KeeperException {

        // zk 是有session的概念，没有连接池的概念的
        // watch 观察回调 watch 只发生在读类型的调用，比如 get exits ...
        // 第一类是new zooKeeper（）的时候传入的，这个watch 是session级别的，跟path，node 没有关系
        //sessionTime  3000  连接超时，也就是代码执行完，3000后断开，如果是临时节点，也会删除
        CountDownLatch countDownLatch = new CountDownLatch(1);
        ZooKeeper zk = new ZooKeeper("192.168.10.11:2181,192.168.10.21:2181,192.168.10.31:2181", 3000,
                new Watcher() {
                    //  回调方法
                    @Override
                    public void process(WatchedEvent watchedEvent) {
                        Event.KeeperState state = watchedEvent.getState();
                        String path = watchedEvent.getPath();
                        Event.EventType type = watchedEvent.getType();
                        System.out.println("new zk watch ===>"+watchedEvent.toString());
                        System.out.println(path);
                        switch (state) {
                            case Unknown:
                                break;
                            case Disconnected:
                                break;
                            case NoSyncConnected:
                                break;
                            case SyncConnected:
                                countDownLatch.countDown();//减一
                                System.out.println("连接成功。。。。。"); //；连接成功的回调
                                break;
                            case AuthFailed:
                                break;
                            case ConnectedReadOnly:
                                break;
                            case SaslAuthenticated:
                                break;
                            case Expired:
                                break;
                        }

                        switch (type) {
                            case None:
                                break;
                            case NodeCreated:
                                break;
                            case NodeDeleted:
                                break;
                            case NodeDataChanged:
                                break;
                            case NodeChildrenChanged:
                                break;
                        }

                    }

                });

        countDownLatch.await();//阻塞
        ZooKeeper.States state = zk.getState();
        switch (state) {
            case CONNECTING:
                System.out.println("ing .......");
                break;
            case ASSOCIATING:
                break;
            case CONNECTED:
                System.out.println("ed ............");
                break;
            case CONNECTEDREADONLY:
                break;
            case CLOSED:
                break;
            case AUTH_FAILED:
                break;
            case NOT_CONNECTED:
                break;
        }

        /**
         * 1、path 是节点的路径
         * 2、节点存放的数据
         * 3、解释：ZooDefs.Ids.OPEN_ACL_UNSAFE：
         *   OPEN_ACL_UNSAFE: 创建任何人都可以操作的节点
         *   READ_ACL_UNSAFE: 创建任何人都可以读的节点
         *   CREATOR_ALL_ACL: 设置了Auth的用户可以使用该ACL集合创建节点，该节点也只能被同样Auth授权的用户操作
         * 4、CreateMode.PERSISTENT : 是创建节点的类型，有 临时节点，临时队列节点， 持久节点，持久队列节点
         */
//        String pathName = zk.create("/jiushu", "oldData".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);

//        Stat stat = new Stat();
//        byte[] node = zk.getData("/jiushu", new Watcher() {
//            //在取数据的时候监听一个回调，取了数据以后，如果这个节点的数据发生变化，则会再次回调这个方法
//            @SneakyThrows
//            @Override
//            public void process(WatchedEvent watchedEvent) {
//                System.out.println("watch ..."+watchedEvent.toString());
//                //zk.getData("/jiushu",true,stat);如果此处写成他，则会回调上面new zk时候的watch
//                zk.getData("/jiushu",this,stat);
//            }
//        }, stat);

//        System.out.println("设置成功"+new String(node).toString());
      //会触发生面get的回调
//        Stat stat1 = zk.setData("/jiushu", "newData".getBytes(), 0);
//
//        //还会会触发生面get的回调吗 ？
//        Stat stat2 = zk.setData("/jiushu", "newData02".getBytes(), stat1.getVersion());

        //异步写法
        System.out.println("--------------------sync start--------------------");
        zk.getData("/jiushu", false, new AsyncCallback.DataCallback() {
            @Override
            public void processResult(int i, String s, Object cxt, byte[] bytes, Stat stat) {
                System.out.println("--------------------sync callBack--------------------");
                System.out.println(cxt.toString()); // 传进来的数据
                System.out.println(new String(bytes));//获取到的数据
            }
        },"abc");// 这里的 abc 就是传进去的值
        System.out.println("--------------------sync end--------------------");
        // 异步的所以需要阻塞在这里等待
        Thread.sleep(20000000);

    }


}
