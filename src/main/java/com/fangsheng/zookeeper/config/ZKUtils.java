package com.fangsheng.zookeeper.config;

import org.apache.zookeeper.ZooKeeper;

import java.util.concurrent.CountDownLatch;

/**
 * @创建人: 放生
 * @创建时间: 2021/11/3
 * @描述:
 */
public class ZKUtils {

    private  static ZooKeeper zk;

    // /testConf 表示这样创建的zooKeeper· 都会以他为根目录，所有的操作都是基于这个目录
    //private static String address = "192.168.10.11:2181,192.168.10.21:2181,192.168.10.31:2181/testConf";
    private static String address = "192.168.10.11:2181,192.168.10.21:2181,192.168.10.31:2181/testLock";

    private static DefaultWatch watch = new DefaultWatch();

    private static CountDownLatch init  =  new CountDownLatch(1);

    public static ZooKeeper  getZK(){

        try {
            zk = new ZooKeeper(address,1000,watch);
            watch.setCc(init);
            init.await();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return zk;
    }





}
